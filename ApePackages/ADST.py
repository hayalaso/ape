"""
Build support for adst_ package
-------------------------------
"""
import os
import os.path  # @UnusedImport (pydev bug)
import shutil

from ApeTools import Config, Build


class ADST(Build.Package):

    builder_default = "autoconf"

    def isInstalled(self):
        """Determine if the adst_ package is installed by looking for
        :program:`EventBrowser`.
        """
        return os.path.exists(self.prefix) and \
            os.path.exists(os.path.join(self.prefix, 'bin')) and \
            os.path.exists(os.path.join(self.prefix, 'bin', 'EventBrowser'))

    def unpack(self):
        """Unpack adst_. The top level directory in the tar-ball is always
        called ``ADST``. We rename it to the requested source-directory.
        Since adst_ does an in-place build, this is also the place of the
        final installation.
        """
        Build.Package.unpack(self)
        if Config.getboolean("ape", "dryRun"):
            return
        if os.path.exists(self.sourceDirectory):
            shutil.rmtree(self.sourceDirectory)
        shutil.move(os.path.join(os.path.dirname(self.sourceDirectory),
                                 "ADST"), self.sourceDirectory)

    def build(self, env):
        """Add the :envvar:`ADSTROOT` environment variable before
        before calling :meth:`ApeTools.Build.Package.build` to build adst_.
        """
        env.update(ADSTROOT=self._env["ADSTROOT"])
        Build.Package.build(self, env)
