Support utilities: the :mod:`ApeTools` package and sub-modules
==============================================================

The :mod:`ApeTools` package and its sub-modules provide utilities to handle the
installation of the Auger Offline software.

.. Installation of the external dependencies and of the the Offline software
   itself.

.. automodule:: ApeTools
   :members:
   :undoc-members:

.. automodule:: ApeTools.Version

.. automodule:: ApeTools.Build
   :members:
   :undoc-members:

.. automodule:: ApeTools.Config
   :members:
   :undoc-members:

.. automodule:: ApeTools.Environment
   :members:

.. automodule:: ApeTools.Fetch
   :members:
   :undoc-members:
